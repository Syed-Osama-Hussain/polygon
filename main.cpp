#include <iostream>
#include <string>
using namespace std;
#include "polygon.h"

int main()
{
  Polygon shape;
  //cout<<"hello";
  Vertex V1(2,3);
  Vertex V2(3,4);
  Vertex V3(4,5);
  Vertex V4(5,4);
  Vertex V5(5,6);
  Vertex V6(7,8);
  Vertex V7(9,9);
  shape.add_Vertex(V1);
  shape.add_Vertex(V2);
  shape.add_Vertex(V3);
  shape.add_Vertex(V4);
  shape.add_Vertex(V5);
  shape.add_Vertex(V6);
  shape.add_Vertex(V7);
  shape.show();
  shape.delete_element(V1);
  shape.show();
  system("pause");


} 