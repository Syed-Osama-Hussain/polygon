#include <iostream>
#include <string>
using namespace std;

class Vertex{
public:

Vertex()
{
  this->x=1.0;
  this->y=2.0;
}

Vertex( float x, float y)
{
  this->x= x;
  this->y= y;
}

Vertex(Vertex& vcopy)
{
  this->x= vcopy.x;
  this->y= vcopy.y;
}

Vertex operator =(float num)
{
  this->x= num;
  this->y= num;
  return *this;
}

void setx(float x)
{
  this->x= x;
}

void sety(float y)
{
  this->y=y;
}

void set(float x, float y)
{
  this->x= x;
  this->y= y;
}

float getx()
{
  return this->x;
}

float gety()
{
  return this->y;
}

friend ostream& operator <<(ostream& out, Vertex& V);



private:
float x;
float y;


};
ostream& operator <<(ostream& out, Vertex& V)
{
  return out<<V.getx()<<endl<<V.gety()<<endl;
}